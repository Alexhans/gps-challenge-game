package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelMenuJugador extends CustomPanel {

	private JButton comenzar;
	private JButton retomar;
	private JButton puntajes;
	private VentanaManager parent;
	
	public PanelMenuJugador(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		comenzar = new JButton("Comenzar nueva partida");
		retomar = new JButton("Retomar ultima partida");
		puntajes = new JButton("Ver los puntajes maximos");
		
		comenzar.addActionListener(parent);
		retomar.addActionListener(parent);
		puntajes.addActionListener(parent);
		
		this.add(comenzar);
		this.add(retomar);
		this.add(puntajes);
		if(!parent.tienePartidasGuardadas()){
			JLabel noTienePartidasGuardadas = new JLabel("Usted no tiene partidas guardadas para retomar.");
			this.add(noTienePartidasGuardadas);
		}
	}

	@Override
	public void procesarAccion(ActionEvent event) {
		if(event.getSource() == comenzar) {
			parent.eliminarPanel(this);
			parent.agregarPanelComenzarPartida();
		}
		if(event.getSource() == retomar) {
			if(parent.tienePartidasGuardadas()){
				parent.eliminarPanel(this);
				parent.agregarVistaPartidaCargada();
			}
		}
		if(event.getSource() == puntajes) {
			parent.eliminarPanel(this);
			parent.agregarPanelPuntajes();
		}
	}

}
