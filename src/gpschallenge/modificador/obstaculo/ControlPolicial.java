package gpschallenge.modificador.obstaculo;

import gpschallenge.Partida;
import gpschallenge.modificador.Modificador;

/**
 * Un obstaculo que actua sobre el puntaje, agregando 3 puntos. Cada vehiculo tiene una probabilidad distinta de ser afectados
 * por el ControlPolicial:
 * <pre> Auto: 0.5
 * Camioneta 4x4: 0.3
 * Moto: 0.8 </pre>
 * @author Santiago
 *
 */
public class ControlPolicial extends Obstaculo {
	@Override
	public Modificador obtenerModificador(Partida partida) {
		return partida.getJugador().getVehiculo().obtenerModificador(this);
	}


}
