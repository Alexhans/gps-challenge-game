package gpschallenge.modificador.sorpresa;

import gpschallenge.Partida;
import gpschallenge.modificador.Modificador;

/**
 * Una sorpresa que al activarse modifica el puntaje restando el 20% de los movimientos.
 *
 */
public class SorpresaFavorable extends Sorpresa {

	@Override
	public Modificador obtenerModificador(Partida partida) {
		Modificador mod = new Modificador();
		mod.productoDeMovimientos = -0.2;
		return mod;
	}
	
}
