package gpschallenge.modificador;

import gpschallenge.Partida;
import gpschallenge.modificador.Modificador;

public interface IEfectoObtenible {
    public Modificador obtenerModificador(Partida partida);
}


