package gpschallenge.mapa;

import java.util.EnumMap;
/** 
 * Cada una de las intersecciones de 4 esquinas en el mapa.
 * */
public class Cruce {
	
	/**
	 * Las direcciones en las cuales, desde un cruce, el vehiculo se puede mover.
	 * 
	 */
	public enum Direccion {
		ARRIBA, ABAJO, IZQUIERDA, DERECHA
	}
	private EnumMap<Direccion, Camino> caminosSalientes;
	// ponerlo como final
	private int identificador;
	
	
	@Override 
	public String toString() {
		return ("Cruce: " + Integer.toString(this.identificador));
	}
	/** 
	 * Sobreescrito para comparar Cruces segun su identificador
	 */
	@Override
	public boolean equals(Object c2) {
		if (c2 == null) return false;
		if (c2 == this) return true;
		if (!(c2 instanceof Cruce)) return false;
		
		Cruce otroCruce = (Cruce)c2;
		if (otroCruce.getIdentificador() == this.getIdentificador()) return true;
		return false;
	}
	
	/**
	 * Crea un Cruce a partir de un numero que servira para identificarlo y diferenciarlo.
	 * @param identificador el numero que lo identifica.
	 */
	public Cruce(int identificador) {
		this.caminosSalientes = new EnumMap<Direccion, Camino>(Direccion.class);
		this.identificador = identificador;
	}
	/**
	 * Devuelve el identificador del cruce.
	 * @return
	 */
	public int getIdentificador() {
		return this.identificador;
	}
	
    /**
     * Agrega un camino a la lista de los disponibles
     * @param caminoAAgregar	
     * @param direccion Cada camino debe ser agregado con una de la siguientes direcciones: ARRIBA, ABAJO, IZQUIERDA, DERECHA
     */
	public void agregarCamino(Camino caminoAAgregar, Direccion direccion) {
		caminosSalientes.put(direccion, caminoAAgregar);
	}
	
	
	/**
	 * 
	 * @param direccion
	 * @return Devuelve el Camino correspondiente a la llave ingresada, o {@code null} si no existe el correspondiente.
	 */
	public Camino getCamino(Direccion direccion) {
		if (caminosSalientes.containsKey(direccion)) { 
            return caminosSalientes.get(direccion); }
		else { return null; }
    }    
	
	
	/**
	 * 
	 * @return Devuelve un EnumMap con todos los caminos posibles desde este cruce, con su correspondiente direccion.
	 */
    public EnumMap<Direccion, Camino> getCaminosSalientes() {
		return this.caminosSalientes;
	}
    /**
     * Se fiaj si existe un camino para una cierta direccion y devuelve el valor booleano correspondiente. 
     * @param direccion La direccion en la cual se busca el camino.
     * @return
     */
	public boolean tieneCaminoEnDireccion(Direccion direccion) {
		return this.caminosSalientes.containsKey(direccion);
    }
}    
