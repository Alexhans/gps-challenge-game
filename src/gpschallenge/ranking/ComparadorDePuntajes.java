package gpschallenge.ranking;

import java.util.Comparator;

public class ComparadorDePuntajes implements Comparator<Puntaje>{
	
	// Cuando dos puntajes son iguales, devuelve -1
	public int compare (Puntaje puntaje1, Puntaje puntaje2) {
		
		if ( puntaje1.getPuntaje() < puntaje2.getPuntaje()){
			return 1;
		} 
		
		return -1;
	}
}
