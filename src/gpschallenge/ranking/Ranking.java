package gpschallenge.ranking;

import java.util.ArrayList;
import java.util.Collections;

public class Ranking {
	
	private ManejadorDeArchivosCSV miManejadorDeArchivos;
	
	/**
	 * Se construye a partir de un archivo csv
	 * @param rutaDelRanking La ruta al archivo.
	 */
	public Ranking(String rutaDelRanking) {
		this.setearManejadorDeArchivos(rutaDelRanking);
	}
	
	public void setearManejadorDeArchivos(String rutaDelRanking){
		this.miManejadorDeArchivos = new ManejadorDeArchivosCSV(rutaDelRanking);
	}
			
	/**
	 * Trata de colocar en el ranking el puntaje que le pasan por parametro, si el puntaje es menor
	 * que los primeros 10 puntajes, no lo guarda, porque solo guarda el TOP 10
	 * @param nuevoPuntaje: Puntaje a tratar de colocar en el Ranking
	 */
	public void colocarEnElRanking(Puntaje nuevoPuntaje){
		int cantidadDePuntajesAMostrar = 10;
		ArrayList<Puntaje> unArrayDePuntajes = miManejadorDeArchivos.obtenerTodoElArchivo();
		unArrayDePuntajes.add(nuevoPuntaje);
		ComparadorDePuntajes unComparador = new ComparadorDePuntajes();
		Collections.sort(unArrayDePuntajes,unComparador);
		// Esto recorta el ranking a las primeras 10 posiciones
		while (unArrayDePuntajes.size() > cantidadDePuntajesAMostrar){
			unArrayDePuntajes.remove(unArrayDePuntajes.size() - 1);
		}
		miManejadorDeArchivos.escribirEnArchivo(unArrayDePuntajes);
	}
	
	/**
	 * Devuelve el ranking en forma de ArrayList de puntajes.
	 * @return un ArrayList con todos los puntajes del ranking, si el ranking no existia, o estaba vacio,
	 *  va a devolver un ArrayList<Puntaje> vacio
	 */
	public ArrayList<Puntaje> getListaPuntajes(){
		return (miManejadorDeArchivos.obtenerTodoElArchivo());
	}
	
}
