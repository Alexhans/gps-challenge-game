package gpschallenge.vehiculo;

import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.obstaculo.Pozo;
import gpschallenge.vehiculo.Auto;

public class Moto extends Vehiculo {
	
	public Moto() {
		super();
		this.setProbabilidadDeSerDemoradoPorControl(0.8);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public IVehiculo vehiculoDeCambio() {
		// TODO Auto-generated method stub
		return new Auto();
	}

	@Override
	public Modificador obtenerModificador(Piquete piquete) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
        mod.adicionDeMovimientos = 2;
        mod.movimientoExitoso = true;
		return mod;
	}


	@Override
	public Modificador obtenerModificador(Pozo pozo) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
        mod.adicionDeMovimientos = 3;
		return mod;
	}
		
}
