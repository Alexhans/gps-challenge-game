package gpschallenge.dificultad;


public class DificultadModerada extends Dificultad {

	public DificultadModerada(String nombre) {
		super(nombre);
	}

	@Override
	public double puntajeEnBaseAMovimientos(double realizados, double sobrantes) {
		return 2.0 * (sobrantes - realizados);
	}

}
