package gpschallenge.dificultad;

import org.junit.Test;

import static org.junit.Assert.*;

public class DificultadTest {
	
	private final double DELTA = 1e-15;

	@Test
	public void testNombre() {
		Dificultad facil = new DificultadFacil("cualquiera");
		assertEquals("cualquiera", facil.getNombre());
	}
	
	@Test
	public void testFacilPuntajeEnBaseAMovimientosFacil() {
		Dificultad facil = new DificultadFacil("facil");
		assertEquals(12.0, facil.puntajeEnBaseAMovimientos(10, 22), this.DELTA);
	}

	@Test
	public void testFacilPuntajeEnBaseAMovimientosModerada() {
		Dificultad mod = new DificultadModerada("mod");
		assertEquals(24.0, mod.puntajeEnBaseAMovimientos(10, 22), this.DELTA);
	}
	
	@Test
	public void testFacilPuntajeEnBaseAMovimientosDificil() {
		Dificultad dificil = new DificultadDificil("dif");
		assertEquals(36.0, dificil.puntajeEnBaseAMovimientos(10, 22), this.DELTA);
	}
	
	@Test
	public void testEqualsConNullEsFalso() {
		Dificultad dificil = new DificultadDificil("dif");
		assertFalse(dificil.equals(null));
	}
	
	@Test
	public void testEqualsConOtraInstancia() {
		Dificultad dificil = new DificultadDificil("dif");
		assertFalse(dificil.equals(new Integer(2)));
	}
	
	@Test 
	public void testEqualsConSiMismoEsTrue() {
		Dificultad dificil = new DificultadDificil("dif");
		assertTrue(dificil.equals(dificil));
	}

	@Test 
	public void testDificultadAPartirDeString() {
		assertEquals(new DificultadFacil("no importa").getClass(), Dificultad.DificultadAPartirDeString("Facil").getClass());
		assertEquals(new DificultadModerada("no importa").getClass(), Dificultad.DificultadAPartirDeString("Moderada").getClass());
		assertEquals(new DificultadDificil("no importa").getClass(), Dificultad.DificultadAPartirDeString("Dificil").getClass());
	}
	
	@Test
	public void testDificultadAPartirDeStringCualquieraDebeDarNull () {
		assertEquals(null, Dificultad.DificultadAPartirDeString("sadjiasdod"));
	}
	
}
