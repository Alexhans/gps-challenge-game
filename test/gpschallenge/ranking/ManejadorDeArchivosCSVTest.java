package gpschallenge.ranking;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.csvreader.CsvWriter;

public class ManejadorDeArchivosCSVTest {
	
	private ManejadorDeArchivosCSV unManejador;
	private final String rutaManejadorDeArchivosTest = "test/gpschallenge/ranking/manejadorDeArchivosTest.csv";;

	@Before
	public final void setUp(){
		crearArchivosCSVParaManejadorDeArchivosTest();
		this.unManejador = new ManejadorDeArchivosCSV(rutaManejadorDeArchivosTest);
	}
	
	@Test
	public void testDeberiaObtenerBienVariosNombres() {
		ArrayList<Puntaje> unArray = new ArrayList<Puntaje>();
		
		unArray.add(new Puntaje("Mica",6));
		unArray.add(new Puntaje("Juan",5));
		unArray.add(new Puntaje("Fede",10));
		
		unManejador.escribirEnArchivo(unArray);
		unArray = unManejador.obtenerTodoElArchivo();
		
		assertEquals((unArray.get(0).getNombre()),"Mica");
		assertEquals((unArray.get(1).getNombre()),"Juan");
		assertEquals((unArray.get(2).getNombre()),"Fede");
	}
	
	@Test
	public void testDeberiaDevolverUnArrayListVacioSiNoExisteElArchivoQueQuieroLeer() {
		ManejadorDeArchivosCSV unManejador = new ManejadorDeArchivosCSV("XXX.csv");
		assertEquals(true, unManejador.obtenerTodoElArchivo().isEmpty());
		
	}
	
	@Test
	public void testDeberiaObtenerBienVariosPuntajes() {
		ArrayList<Puntaje> unArray = new ArrayList<Puntaje>();
		
		unArray.add(new Puntaje("Mica",6));
		unArray.add(new Puntaje("Juan",5));
		unArray.add(new Puntaje("Fede",10));
		
		unManejador.escribirEnArchivo(unArray);
		unArray = unManejador.obtenerTodoElArchivo();
		
		assertEquals((unArray.get(0).getPuntaje()), 6 , 0.1);
		assertEquals((unArray.get(1).getPuntaje()), 5 , 0.1);
		assertEquals((unArray.get(2).getPuntaje()), 10 , 0.1);
	}
	
	@Test
	public void testDeberiaObtenerBienVariosNombresYPuntajes() {
		ArrayList<Puntaje> unArray = new ArrayList<Puntaje>();
		
		unArray.add(new Puntaje("Mica",6));
		unArray.add(new Puntaje("Juan",5));
		unArray.add(new Puntaje("Fede",10));
		
		unManejador.escribirEnArchivo(unArray);
		unArray = unManejador.obtenerTodoElArchivo();
		
		assertEquals((unArray.get(0).getNombre()),"Mica");
		assertEquals((unArray.get(1).getNombre()),"Juan");
		assertEquals((unArray.get(2).getNombre()),"Fede");
		assertEquals((unArray.get(0).getPuntaje()), 6 , 0.1);
		assertEquals((unArray.get(1).getPuntaje()), 5 , 0.1);
		assertEquals((unArray.get(2).getPuntaje()), 10 , 0.1);
	}
	
	@Test
	public void testDeberiaEscribirBienElNombre() {
		ArrayList<Puntaje> unArray = new ArrayList<Puntaje>();
		unArray.add(new Puntaje("Juan",6));
		unManejador.escribirEnArchivo(unArray);
		assertEquals((unArray.get(0).getNombre()),"Juan");
	}
	
	@Test
	public void testDeberiaEscribirBienElPuntaje() {
		ArrayList<Puntaje> unArray = new ArrayList<Puntaje>();
		unArray.add(new Puntaje("Juan",6));
		unManejador.escribirEnArchivo(unArray);
		assertEquals((unArray.get(0).getPuntaje()), 6 , 0.1);
	}
	
	@After 
	public void tearDown(){
		File fileABorrar = new File(rutaManejadorDeArchivosTest);
		fileABorrar.delete();
	}
	
	public void crearArchivosCSVParaManejadorDeArchivosTest(){
		CsvWriter escritorCsv = null;
		try {
            // Crea el escritor para archivos CSV. Al poner false por parametro, sobreescribe el archivo. Tambien le introduce las cabeceras
            escritorCsv = new CsvWriter(new FileWriter(rutaManejadorDeArchivosTest, false), ',');
           	escritorCsv.write("Nombre");
           	escritorCsv.write("Puntaje");
           	escritorCsv.endRecord(); 
            escritorCsv.close();
         
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	escritorCsv.close();
        }
	}
}
