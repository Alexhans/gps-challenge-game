package gpschallenge.vehiculo;

import static org.junit.Assert.*;

import java.util.HashMap;

import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.ControlPolicial;

import org.easymock.EasyMock;
import org.powermock.api.easymock.PowerMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest( {Camioneta4x4.class} )
public class Camioneta4x4Test {

	private final double DELTA = 1e-15;
	
	@Test
	public void testCambioDeVehiculo() {
		IVehiculo unVehiculo = new Camioneta4x4(); 

		org.junit.Assert.assertSame((new Moto()).getClass(), unVehiculo.vehiculoDeCambio().getClass());
	}
	
	@Test
	public void testProbabilidadDeSerDemoradoEnBaseAVehiculo() {
		  HashMap<Vehiculo, Double> mapaVehicProb = new HashMap<Vehiculo, Double>();
		  mapaVehicProb.put(new Camioneta4x4(), 0.3);
		  mapaVehicProb.put(new Auto(), 0.5);
		  mapaVehicProb.put(new Moto(), 0.8);
		  
		  for(java.util.Map.Entry<Vehiculo, Double> entry : mapaVehicProb.entrySet()) {
			  Vehiculo vehiculo = entry.getKey();
			  double probablididad = entry.getValue();
		      PowerMock.mockStaticPartial(Math.class, "random");
		      EasyMock.expect (Math.random()).andReturn(probablididad - this.DELTA).anyTimes();
		      PowerMock.replayAll();
		      Modificador modif = new Modificador();
		      modif.adicionDeMovimientos = 3;
		      assertEquals(modif, vehiculo.obtenerModificador(new ControlPolicial()));
		      PowerMock.verifyAll();	
		  }
	}


}
