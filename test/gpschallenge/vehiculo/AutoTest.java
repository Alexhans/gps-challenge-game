package gpschallenge.vehiculo;

import org.junit.Test;

public class AutoTest {

	@Test
	public void testCambioDeVehiculo() {
		IVehiculo unVehiculo = new Auto(); 

		org.junit.Assert.assertSame((new Camioneta4x4()).getClass(), unVehiculo.vehiculoDeCambio().getClass());
	}

}
