package gpschallenge.mapa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import gpschallenge.modificador.IEfectoObtenible;
import gpschallenge.modificador.obstaculo.Obstaculo;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.sorpresa.Sorpresa;
import gpschallenge.modificador.sorpresa.SorpresaCambioDeVehiculo;
import gpschallenge.modificador.sorpresa.SorpresaDesfavorable;

import org.junit.Test;


public class CaminoTest {
	@Test
	public void testConstruirCamino() {
		@SuppressWarnings("unused")
		Camino unCamino = new Camino(new Cruce(1), new Cruce(2));
	}
	
	@Test
	public void testContieneA() {
		Cruce uno = new Cruce(1);
		Cruce dos = new Cruce(2);
		Cruce tres = new Cruce(3);
		Camino unCamino = new Camino(uno, dos);
		assertTrue(unCamino.contieneA(uno));
		assertTrue(unCamino.contieneA(dos));
		assertFalse(unCamino.contieneA(tres));
	}
	
	@Test
	public void testEquals() {
		Cruce uno = new Cruce(1);
		Cruce dos = new Cruce(2);
		Cruce aaa = new Cruce(1);
		Cruce bbb = new Cruce(2);
		Camino unCamino = new Camino(uno, dos);
		Camino otroCamino = new Camino(aaa, bbb);
		assertEquals(unCamino, otroCamino);
		assertEquals(unCamino, new Camino(new Cruce(1), new Cruce(2)));
	}
	
	@Test 
	public void testElOrdenDeLosCruceNoDebeImportar() {
		Cruce uno = new Cruce(1);
		Cruce dos = new Cruce(2);
		Camino unCamino = new Camino(uno, dos);
		assertEquals(unCamino, new Camino(dos, uno));
		assertEquals(unCamino, new Camino(new Cruce(2), new Cruce(1)));
	}
	
	@Test 
	public void testSiUnCaminoEsDiferenteEqualsDebeSerFalso() {
		Cruce uno = new Cruce(1);
		Cruce dos = new Cruce(2);
		Camino unCamino = new Camino(uno, dos);
		assertFalse(unCamino.equals(new Camino(new Cruce(3), uno)));
	}
	
	@Test 
	public void testAgregarEfecto() {
		IEfectoObtenible efecto = new SorpresaCambioDeVehiculo();
		Cruce cruceUno = new Cruce(1);
		Camino unCamino = new Camino(cruceUno, new Cruce(2));
		unCamino.agregarEfecto(efecto);
		assertEquals(efecto, unCamino.getEfectosDesde(cruceUno).get(0));
		assertEquals(1, unCamino.getEfectosDesde(cruceUno).size());
		unCamino.eliminarEfecto(efecto);
		assertEquals(0, unCamino.getEfectosDesde(cruceUno).size());
		
	}
	
	@Test
	public void devuelveEfectosEnOrdenSiVengoDesdeElCruceIzquierdo(){ // Izquierdo y superior es lo mismo
		Cruce cruceUno = new Cruce(1);
		Cruce cruceDos = new Cruce(2);
		Camino unCamino = new Camino(cruceUno,cruceDos);
		// Agrega los efectos segunda la convencion, primero la sorpresa y despues el obstaculo
		Sorpresa sorpresa = new SorpresaDesfavorable();
		Obstaculo piquete = new Piquete();
		unCamino.agregarEfecto(sorpresa);
		unCamino.agregarEfecto(piquete);
		// Obtengo los efectos viniendo desde la izquierda
		ArrayList<IEfectoObtenible> listaEfectos = unCamino.getEfectosDesde(cruceUno);
		assertEquals(listaEfectos.get(0),sorpresa);
		assertEquals(listaEfectos.get(1),piquete);
	}
	
	@Test
	public void devuelveEfectosEnOrdenSiVengoDesdeElCruceDerecho(){ // Derecho e inferior es lo mismo
		Cruce cruceUno = new Cruce(1);
		Cruce cruceDos = new Cruce(2);
		Camino unCamino = new Camino(cruceUno,cruceDos);
		// Agrega los efectos segunda la convencion, primero la sorpresa y despues el obstaculo
		Sorpresa sorpresa = new SorpresaDesfavorable();
		Obstaculo piquete = new Piquete();
		unCamino.agregarEfecto(sorpresa);
		unCamino.agregarEfecto(piquete);
		// Obtengo los efectos viniendo desde la derecha
		ArrayList<IEfectoObtenible> listaEfectos = unCamino.getEfectosDesde(cruceDos);
		assertEquals(listaEfectos.get(0),piquete);
		assertEquals(listaEfectos.get(1),sorpresa);
	}
	// Sorpresa/Obstaculo va a ser una instancia asi que lo hago despues.
	
	@Test
	public void setCaminoFalse(){
		Camino camino = new Camino(new Cruce(0), new Cruce(1));
		camino.setVisible(true);
		assertEquals(camino.estaVisible(), true);
	}
	
	@Test 
	public void creaLosCaminosConVisibilidadIgualAFalse(){
		Camino camino = new Camino(new Cruce(0), new Cruce(1));
		assertEquals(camino.estaVisible(), false);
	}
	
	@Test
	public void enUnaListaDeCaminosSoloAlgunosTienenVisibilidadTrue(){
		ArrayList<Camino> listaDeCaminos = new ArrayList<Camino>();
		for (int i = 0 ; i<4 ; ++i){
			Camino camino = new Camino(new Cruce(i), new Cruce(i+1));
			listaDeCaminos.add(camino);
		}
		listaDeCaminos.get(1).setVisible(true);
		listaDeCaminos.get(3).setVisible(true);
		assertEquals(listaDeCaminos.get(0).estaVisible(), false);
		assertEquals(listaDeCaminos.get(1).estaVisible(), true);
		assertEquals(listaDeCaminos.get(2).estaVisible(), false);
		assertEquals(listaDeCaminos.get(3).estaVisible(), true);
	}
	@Test(expected = NoSuchElementException.class)
	public void testDeberiaLanzarExcepcionAlPedirEfectosACaminoConCruceInvalido() {
		Cruce cruceA = new Cruce(1);
		Cruce cruceB = new Cruce(2);
		Cruce cruceNoContenido = new Cruce(3);
		Camino camino = new Camino(cruceA, cruceB);
		camino.getEfectosDesde(cruceNoContenido);
	}
}
